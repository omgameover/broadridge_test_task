import React from "react";
import PropTypes from "prop-types";

import IconClose from "../IconClose";
import Badge from "../Badge";
import "./tile.css";

export default function Tile({ onClose, img, title, description, badgeText }) {
  return (
    <div className="tile-container" style={{ backgroundImage: `url(${img})` }}>
    <div className="overlay" />
      <div className="icon-close-container">
        <IconClose onClick={onClose} />
      </div>
      <div className="title-container">
        <Badge text={badgeText} />
        <div className="title">{title}</div>
        <div className="description">{description}</div>
      </div>
    </div>
  );
}

Tile.propTypes = {
  onClose: PropTypes.func.isRequired,
  img: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  badgeText: PropTypes.string
};
