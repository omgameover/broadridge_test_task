import React from "react";
import PropTypes from "prop-types";

import "./badge.css";

export default function Badge({ text }) {
  if (!text) return null;
  return <div className="badge">{text}</div>;
}

Badge.propTypes = {
  text: PropTypes.string
};
