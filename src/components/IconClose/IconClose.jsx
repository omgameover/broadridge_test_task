import React from "react";
import PropTypes from "prop-types";

import "./iconClose.css";

export default function IconClose({ onClick = () => {} }) {
  return (
    <div className="icon-close" onClick={onClick}>
      ✕
    </div>
  );
}

IconClose.propTypes = {
  onClose: PropTypes.func
};
