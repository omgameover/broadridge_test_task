import React, { useState } from "react";

import Tile from "./components/Tile";
import background1 from "./resources/background1.png";
import background2 from "./resources/background2.png";
import "./App.css";

const TILE_PATTERN = {
  title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  badgeText: "New Product"
};

const TILES = [
  { ...TILE_PATTERN, img: background1 },
  { ...TILE_PATTERN, img: background2, badgeText: null }
];

function App() {
  const [tiles, setTiles] = useState([...TILES]);
  const handleClose = id =>
    setTiles(prevTiles => [...prevTiles.filter((_, index) => index !== id)]);

  return (
    <div className="app-container">
      {tiles.map((tileProps, key) => (
        <Tile key={key} {...tileProps} onClose={() => handleClose(key)} />
      ))}
    </div>
  );
}

export default App;
